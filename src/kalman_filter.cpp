#include <iostream>
#include <math.h> 
#include "tools.h"
#include "kalman_filter.h"

using Eigen::MatrixXd;
using Eigen::VectorXd;



using namespace std;

#define PI 3.14159265

KalmanFilter::KalmanFilter() {}

KalmanFilter::~KalmanFilter() {}

void KalmanFilter::Init(VectorXd &x_in, MatrixXd &P_in, MatrixXd &F_in,
                        MatrixXd &H_in, MatrixXd &R_in, MatrixXd &Q_in) {
  x_ = x_in;
  P_ = P_in;
  F_ = F_in;
  H_ = H_in;
  R_ = R_in;
  Q_ = Q_in;

  
}

void KalmanFilter::Predict() {
  /**
  TODO:
    * predict the state
  */
  x_ = F_*x_;
  P_ = F_*P_*F_.transpose() + Q_;
}

void KalmanFilter::Update(const VectorXd &z) {
  /**
  TODO:
    * update the state by using Kalman Filter equations
  */

  VectorXd y_ = z - H_*x_;
  MatrixXd S_ = H_*P_*H_.transpose() + R_;
  MatrixXd K_ = P_*H_.transpose()*S_.inverse();

  x_ = x_ + K_*y_;
  I_ = MatrixXd::Identity(x_.size(), x_.size());
  P_ = (I_ - K_*H_)*P_;

  
}


double normalize_phi(double phi) {
    if (phi < -PI) {
      while(phi < -PI) {
        phi += 2*PI;
      }
    }
    else if (phi > PI) {
      while(phi > PI) {
        phi -= 2*PI;
      }
    }
    return phi;
}

VectorXd calculte_h_(const VectorXd &x_state) {
    VectorXd h_ = VectorXd(3);
    float px = x_state(0);
    float py = x_state(1);
    float vx = x_state(2);
    float vy = x_state(3);

    float c1 = px*px+py*py;
    float c2 = sqrt(c1);
    float rho = c2;
    float rho_dot;


  if (fabs(rho) < 0.0001) {
    rho_dot = 0;
  } else {
    rho_dot = (px*vx + py*vy)/rho;
  }

    // atan2 returns values in range: [-pi, pi]
    float phi = atan2(py,px);
    normalize_phi(phi);

    

    h_ << rho, phi, rho_dot;

    return h_;
 
}

void KalmanFilter::UpdateEKF(const VectorXd &z) {
  /**
  TODO:
    * update the state by using Extended Kalman Filter equations
  */
  VectorXd h_ = calculte_h_(x_);
  VectorXd y_ = z - h_;
  y_(1) = normalize_phi(y_(1));
  MatrixXd S_ = H_*P_*H_.transpose() + R_;
  MatrixXd K_ = P_*H_.transpose()*S_.inverse();

  x_ = x_ + K_*y_;
  I_ = MatrixXd::Identity(x_.size(), x_.size());
  P_ = (I_ - K_*H_)*P_;

}
